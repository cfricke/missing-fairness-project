from collections import defaultdict
from os import makedirs, path

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


class ConditionalDiscrimination:
    """Conditional discrimination analysis."""

    def __init__(
        self, df, sensitives_mapping=None, explainables=None, label="accepted", target=1
    ):
        self.df = df
        self.sm = sensitives_mapping
        self.es = explainables
        self.label = label
        self.target = target

        self._alls = {}
        self._expl = defaultdict(dict)
        self._bads = defaultdict(dict)

    @property
    def alls(self):
        return self._alls

    @property
    def bads(self):
        return self._bads

    @property
    def explainables(self):
        return self._expl

    @classmethod
    def _spd(cls, df, sensitive_attr, sensitive_values, label, target):
        """
        Compute the statistical parity difference, which forms the base discrimination.

        """
        sa = sensitive_attr
        sv = sensitive_values

        if len(sv) != 2:
            raise Exception(
                f"{sa} requires a binary set of unique values, but was {sv}"
            )

        # P(+|s0) - P(+|s1)
        p0 = len(df[(df[label] == target) & (df[sa] == sv[0])]) / len(
            df[df[sa] == sv[0]]
        )
        p1 = len(df[(df[label] == target) & (df[sa] == sv[1])]) / len(
            df[df[sa] == sv[1]]
        )
        return p0 - p1

    def compute(self):
        """
        Compute all, explainable and bad discrimination for all sensitive values and
        explainable attributes.  This operation may take a while depending on the size
        of the dataset.  All functions support caching of results, instead, and do not
        require any recomputation.

        """

        if self.sm is None or self.es is None:
            raise Exception(
                "compute() requires sensitives_mapping and explainables to be set\n"
                f"Values are respectively: {self.sm} and {self.es}"
            )

        for sa, sv in self.sm.items():
            # Exclude sensitive attribute from set of explainable variable
            for ex in set(self.es) - set([sa]):
                self.bad(sa, sv, ex)

    def all(self, sa, sv):
        if self._alls.get(sa) is None:
            self._alls[sa] = self._spd(self.df, sa, sv, self.label, self.target)

        return self._alls[sa]

    def explainable(self, sa, sv, ex):
        if len(sv) != 2:
            raise Exception(
                f"{sa} requires a binary set of unique values of"
                f" (unprivileged, privileged), but was {sv}"
            )

        if self._expl.get(sa, {}).get(ex) is not None:
            return self._expl[sa][ex]

        self._expl[sa][ex] = 0
        for v in self.df[ex].unique():
            # Σ{i} (P(e_i|s0) - P(e_i|s1)) * P(+|e_i)
            ps0 = len(self.df[(self.df[ex] == v) & (self.df[sa] == sv[0])]) / len(
                self.df[self.df[sa] == sv[0]]
            )
            ps1 = len(self.df[(self.df[ex] == v) & (self.df[sa] == sv[1])]) / len(
                self.df[self.df[sa] == sv[1]]
            )
            pe1 = len(self.df[(self.df[ex] == v) & (self.df[sa] == sv[0])])
            if pe1 > 0:
                pe1 = (
                    len(
                        self.df[
                            (self.df[self.label] == self.target)
                            & (self.df[ex] == v)
                            & (self.df[sa] == sv[0])
                        ]
                    )
                    / pe1
                )

            pe2 = len(self.df[(self.df[ex] == v) & (self.df[sa] == sv[1])])
            if pe2 > 0:
                pe2 = (
                    len(
                        self.df[
                            (self.df[self.label] == self.target)
                            & (self.df[ex] == v)
                            & (self.df[sa] == sv[1])
                        ]
                    )
                    / pe2
                )

            self._expl[sa][ex] += (ps0 - ps1) * ((pe1 + pe2) / 2)

        return self._expl[sa][ex]

    def bad(self, sa, sv, ex):
        if self._bads.get(sa, {}).get(ex) is not None:
            return self._bads[sa][ex]

        if self._alls.get(sa) is None:
            self.all(sa, sv)

        if self._expl.get(sa, {}).get(ex) is None:
            self.explainable(sa, sv, ex)

        self._bads[sa][ex] = self._alls[sa] - self._expl[sa][ex]
        return self._bads[sa][ex]

    def plot(self, dirpath):
        makedirs(dirpath, exist_ok=True)
        filepaths = []
        for sa in self.sm.keys():
            fig = plt.figure(figsize=(7, 7))
            with sns.axes_style("ticks"):
                data = pd.DataFrame(
                    [self.bads[sa].values(), [self.alls[sa]] * len(self.bads[sa])],
                    columns=self.bads[sa].keys(),
                    index=["d_bad", "d_all"],
                ).T.sort_index()
                axes = fig.add_subplot()
                sns.lineplot(
                    data=data,
                    palette=sns.color_palette("Set1", 2),
                    markers={"d_bad": "o", "d_all": ","},
                    legend="full",
                    ax=axes,
                )
                # Remove spines
                sns.despine(top=True)
                plt.title(sa, fontsize=12, pad=10)
                plt.xlim(-0.1, len(self.es) - 0.9)
                # Set text labels
                axes.tick_params(length=4, labelsize=12)
                plt.xticks(rotation=45, ha="right")
                plt.xlabel("Explanatory Attribute", weight="bold", labelpad=10)
                plt.ylabel("Discrimination", weight="bold", labelpad=10)
                plt.tight_layout()

            fpath = path.join(dirpath, f"conditional_discrimination_{sa}.png")
            filepaths.append(fpath)
            fig.savefig(fpath, dpi=300)
            plt.close()

        return filepaths
