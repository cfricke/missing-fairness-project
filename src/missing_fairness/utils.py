import random
from copy import deepcopy
from os import makedirs, path
from typing import Any, Dict, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from sklearn.experimental import enable_iterative_imputer  # noqa
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import KNNImputer, IterativeImputer, SimpleImputer
from sklearn.linear_model import BayesianRidge
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    f1_score,
    matthews_corrcoef,
    precision_score,
    recall_score,
)
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import OrdinalEncoder

from missing_fairness.mdot_utils import MAR_mask, MNAR_mask_logistic
from missing_fairness.metrics import ConditionalDiscrimination

sns.set(style="whitegrid", palette="muted", color_codes=True)


class Dataset:
    def __init__(
        self,
        df: pd.DataFrame,
        label,
        attrs_encd=None,
        attrs_incl=None,
        senstv_map=None,
    ):
        self.df = df.copy()
        self.label = label
        self.attrs_encd = attrs_encd
        self.attrs_incl = attrs_incl
        self.senstv_map = senstv_map

        if attrs_encd is not None:
            self.encode()

    @property
    def attrs_sens(self):
        return None if self.senstv_map is None else list(self.senstv_map.keys())

    @property
    def attrs_miss(self):
        return self.df.columns[self.df.isnull().any()]

    @property
    def complete_cases(self, all_attrs=False):
        subset = self.df.columns if all_attrs else self.attrs_incl + self.attrs_sens
        return self.df.dropna(subset=subset)

    @property
    def incomplete_cases(self):
        return self.df.loc[self.df.index.difference(self.complete_cases.index), :]

    def copy(self):
        return deepcopy(self)

    def encode(self):
        encoder = OrdinalEncoder(dtype=np.float64)
        for a in self.attrs_encd:
            encoded = encoder.fit_transform(
                np.array(self.df[a].dropna()).reshape(-1, 1)
            )
            self.df.loc[self.df[a].notnull(), a] = np.squeeze(encoded)
            self.df[a] = self.df[a].astype(np.float64)

    def sample_attrs(self, attrs=None, num_attrs=3, random_state=None):
        if random_state is not None:
            random.seed(random_state)

        if attrs is None:
            attrs = self.attrs_incl

        return random.sample(attrs, num_attrs) if len(attrs) >= num_attrs else attrs


def plot_correlations(ds: Dataset, dirpath: str, filename_suffix=None) -> str:
    na_ratios = ds.df[ds.attrs_incl].isna().sum() / len(ds.df)
    na_ratios.sort_values(ascending=False, inplace=True)
    na_ratios = pd.concat(
        [
            na_ratios[na_ratios > 0],
            na_ratios[na_ratios == 0].sort_index(ascending=True),
        ]
    )
    merged_na_corrs = ds.df[na_ratios.index].iloc[
        :,
        [
            i
            for i, n in enumerate(np.var(ds.df[na_ratios.index].isnull(), axis="rows"))
            if n > 0
        ],
    ]
    merged_na_corrs = merged_na_corrs.isna().astype(int)
    merged_na_corrs = pd.concat(
        [
            merged_na_corrs.drop(ds.attrs_sens, axis=1, errors="ignore"),
            ds.df[ds.attrs_sens + [ds.label]],
        ],
        axis=1,
    )
    for s in ds.attrs_sens:
        merged_na_corrs[s] = 1 - (merged_na_corrs[s] != ds.senstv_map[s][0]).astype(int)

    merged_na_corrs = merged_na_corrs.corr()
    mask = np.zeros_like(merged_na_corrs)
    mask[np.triu_indices_from(mask)] = True
    # np.fill_diagonal(mask, 0)

    # Plot
    fig, axes = plt.subplots(figsize=(9, 7))
    sns.heatmap(
        merged_na_corrs,
        mask=mask,
        cbar=True,
        vmin=-1,
        vmax=1,
        cmap="RdBu",
        annot=True,
        annot_kws={"size": 7.5},
        fmt=".2f",
        xticklabels=True,
        yticklabels=True,
        square=True,
    )

    # Set text labels
    axes.tick_params(labelsize=10)
    # fig.suptitle("Missing Value Correlations", fontsize=15, y=0.95)
    plt.xticks(rotation=45, ha="right")
    plt.title("Missing Value Correlations", fontsize=15, pad=20)
    plt.tight_layout()

    # Save plot
    makedirs(dirpath, exist_ok=True)
    fsuffix = f"_{filename_suffix}" if filename_suffix is not None else ""
    fpath = path.join(dirpath, f"correlations{fsuffix}.png")
    fig.savefig(fpath, dpi=300)
    plt.close()

    return fpath


def plot_histogram(ds: Dataset, dirpath: str, filename_suffix=None) -> str:
    na_ratios = ds.df[ds.attrs_incl + ds.attrs_sens].isna().sum() / len(ds.df)
    na_ratios.sort_values(ascending=False, inplace=True)
    na_ratios = pd.concat(
        [
            na_ratios[na_ratios > 0],
            na_ratios[na_ratios == 0].sort_index(ascending=True),
        ]
    )
    grid_kws = {"height_ratios": (1, 24), "hspace": 0.03}
    fig, axes = plt.subplots(2, figsize=(7, 9), gridspec_kw=grid_kws)
    with sns.axes_style("whitegrid"):
        sns.barplot(
            x=na_ratios,
            y=na_ratios.index,
            linewidth=0.75,
            edgecolor="black",
            color=sns.color_palette()[3],
            ax=axes[1],
        )

    # Remove spines
    sns.despine(top=False, bottom=True, left=True, offset=5)
    axes[0].axis("off")
    axes[1].spines["top"].set_color("black")
    axes[1].grid(False)
    axes[1].xaxis.tick_top()
    axes[1].set_xlim(xmax=0.7)
    axes[1].tick_params(labelsize=12)

    # Set text labels
    fig.suptitle("Proportion of Missingness", fontsize=20, y=0.95)
    plt.gcf().subplots_adjust(left=0.22)
    # plt.tight_layout()

    # Save plot
    makedirs(dirpath, exist_ok=True)
    fsuffix = f"_{filename_suffix}" if filename_suffix is not None else ""
    fpath = path.join(dirpath, f"histogram{fsuffix}.png")
    fig.savefig(fpath, dpi=300)
    plt.close()

    return fpath


def plot_aggregation(
    ds: Dataset, dirpath: str, filename_suffix=None, threshold: float = 0.01
) -> str:
    na_ratios = ds.df[ds.attrs_incl + ds.attrs_sens].isna().sum() / len(ds.df)
    na_ratios.sort_values(ascending=False, inplace=True)
    na_ratios = pd.concat(
        [
            na_ratios[na_ratios > 0],
            na_ratios[na_ratios == 0].sort_index(ascending=True),
        ]
    )
    patterns = (
        ds.df[ds.attrs_incl + ds.attrs_sens]
        .isna()
        .groupby(list(na_ratios.index))
        .size()
        .reset_index()
        .rename(columns={0: "records"})
    )
    patterns.sort_values(by="records", ascending=False, inplace=True)
    patterns = patterns[patterns.records > (len(ds.df) * threshold)].reset_index(
        drop=True
    )

    # Plot data
    grid_kws = {"height_ratios": (1, 20), "hspace": 0.03}
    fig, axes = plt.subplots(2, figsize=(7, 9), gridspec_kw=grid_kws)
    with sns.axes_style("white"):
        sns.barplot(
            x=patterns.records,
            y=patterns.index,
            linewidth=0.75,
            edgecolor="black",
            color=sns.color_palette()[3],
            ax=axes[0],
        )
        sns.heatmap(
            patterns[list(na_ratios.index)].transpose(),
            linecolor="k",
            linewidth=0.5,
            cbar=False,
            cmap=["white", sns.color_palette()[3]],
            xticklabels=False,
            yticklabels=True,
            ax=axes[1],
        )

    # Remove spines
    sns.despine(top=True, bottom=True, right=True, left=True)
    axes[0].grid(False)
    axes[0].set_xlabel("")
    axes[0].set_xticks([])
    axes[0].set_yticks([])
    axes[1].set_xlabel("")
    axes[1].tick_params(labelsize=12)

    # Set text labels
    fig.suptitle("Missingness Pattern", fontsize=20, y=0.95)
    plt.gcf().subplots_adjust(left=0.22)
    # plt.tight_layout()

    # Save plot
    makedirs(dirpath, exist_ok=True)
    fsuffix = f"_{filename_suffix}" if filename_suffix is not None else ""
    fpath = path.join(dirpath, f"aggregation{fsuffix}.png")
    fig.savefig(fpath, dpi=300)
    plt.close()

    return fpath


def score(
    df: pd.DataFrame,
    label: str,
    imputation: str,
    attrs_incl: List[str],
    attrs_sens: List[str],
    sv_map: Dict[str, List],
    target: Any = 1,
):
    m = RandomForestClassifier(
        n_estimators=50,
        criterion="entropy",
        max_depth=50,
        min_samples_split=10,
        min_samples_leaf=2,
        max_features=0.5,
        n_jobs=-1,
        random_state=0,
        class_weight="balanced",
        # ccp_alpha=1e-15,
        # max_samples=0.75,
    )
    sss = StratifiedShuffleSplit(
        n_splits=10, test_size=1 / np.sqrt(len(attrs_incl)), random_state=0
    )
    scores = []
    splits = []
    d_alls = []
    c_mats = []
    d_all = ConditionalDiscrimination._spd
    for tr_idx, te_idx in sss.split(df[attrs_incl], df[label]):
        dtr = df.iloc[tr_idx, :]
        dte = df.iloc[te_idx, :]
        ytr = dtr[label].values
        xtr = dtr[attrs_incl].values
        yte = dte[label].values
        xte = dte[attrs_incl].values

        # Splits
        splits.append((sum(yte == target), len(yte)))

        # Missing value imputation
        if imputation is not None:
            if imputation == "knn":
                imputer = KNNImputer(n_neighbors=5)
            elif imputation == "iterative":
                imputer = IterativeImputer(
                    estimator=BayesianRidge(),
                    sample_posterior=True,
                    max_iter=10,
                    initial_strategy="most_frequent",
                    random_state=0,
                )
            elif imputation == "simple":
                imputer = SimpleImputer(strategy="most_frequent")
            else:
                raise ValueError(f'Imputation method "{imputation}" unknown.')

            imputer.fit(xtr)
            xtr = imputer.transform(xtr)
            xte = imputer.transform(xte)

        # Performance Measures: accuracy, etc.
        m.fit(xtr, ytr)
        ypr = m.predict(xte)
        scores.append(
            {
                "acc": accuracy_score(yte, ypr),
                "pre": precision_score(yte, ypr),
                "rec": recall_score(yte, ypr),
                "f1s": f1_score(yte, ypr),
                "mat": matthews_corrcoef(yte, ypr),
            }
        )

        # Discrimination
        dpr = dte.copy()
        dpr["predicted"] = ypr
        ds = {}
        for s, sv in sv_map.items():
            ds[s] = d_all(dpr, s, sv_map[s], label="predicted", target=target)
            ds["d_" + s] = d_all(dpr, s, sv_map[s], label=label, target=target)
            ds["n_" + s] = len(dpr[dpr[s] == sv[1]])

        d_alls.append(ds)

        # Normalized Confusion Matrix
        cm = confusion_matrix(yte, ypr)
        c_mats.append(cm.astype("float") / cm.sum(axis=1)[:, np.newaxis])

    return (splits, d_alls, scores, c_mats)


def print_scores(attrs_sens, splits, d_alls, scores, c_mats):
    def print_tab(*args: str) -> None:
        print("|", " | ".join(args), "|")

    print("|-|")
    print_tab("", *["<r>"] * 2)
    print("|-|")
    p_mean, n_mean = np.mean([(p, t - p) for p, t in splits], axis=0)
    print_tab("avg. samples", "", f"{p_mean + n_mean:.0f}")
    print_tab("avg. split (p/n)", f"{p_mean:.0f}", f"{n_mean:.0f}")
    print("|-|")
    for s in attrs_sens:
        d_var = np.var([d["d_" + s] for d in d_alls])
        d_mean = np.mean([d["d_" + s] for d in d_alls])
        d_mean_n = np.mean([d["n_" + s] for d in d_alls])
        print_tab(f"spd d. ({s})", f"{d_mean_n:.0f}", f"{d_mean:.4f} ± {d_var:.4f}")

    print("|-|")
    for s in attrs_sens:
        d_var = np.var([d[s] for d in d_alls])
        d_mean = np.mean([d[s] for d in d_alls])
        d_mean_n = np.mean([d["n_" + s] for d in d_alls])
        print_tab(f"spd m. ({s})", f"{d_mean_n:.0f}", f"{d_mean:.4f} ± {d_var:.4f}")

    print("|-|")
    accs = [i["acc"] for i in scores]
    print_tab("accuracy", "", f"{np.mean(accs):.4f} ± {np.var(accs):.4f}")
    pres = [i["pre"] for i in scores]
    print_tab("precision", "", f"{np.mean(pres):.4f} ± {np.var(pres):.4f}")
    recs = [i["rec"] for i in scores]
    print_tab("recall", "", f"{np.mean(recs):.4f} ± {np.var(recs):.4f}")
    f1ss = [i["f1s"] for i in scores]
    print_tab("f1 score", "", f"{np.mean(f1ss):.4f} ± {np.var(f1ss):.4f}")
    mats = [i["mat"] for i in scores]
    print_tab("matthews corr. coef", "", f"{np.mean(mats):.4f} ± {np.var(mats):.4f}")
    print("|-|")
    cm = np.mean(c_mats, axis=0)
    tn, fp, fn, tp = map(lambda x: f"{x:.3f}", cm.ravel())
    print_tab("conf. matrix", "Y = 1", "Y = 0")
    print_tab("Ŷ = 1", str(tp), str(fp))
    print_tab("Ŷ = 0", str(fn), str(tn))
    print("|-| ")


def gen_nan(df, p_miss, mechanism="MCAR", p_obs=0.5, p_params=0.3):
    X = torch.from_numpy(df.to_numpy().astype(np.float32))

    if mechanism == "MCAR":
        mask = (torch.rand(X.shape) < p_miss).double()
    elif mechanism == "MAR":
        mask = MAR_mask(X, p_miss, p_obs).double()
    elif mechanism == "MNAR":
        mask = MNAR_mask_logistic(X, p_miss, p_params=p_params).double()
    else:
        raise Exception("Unknow missingness mechanism")

    X[mask.bool()] = np.nan
    return X
